from __future__ import unicode_literals

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.safestring import mark_safe
from django.conf import settings
import datetime
import os



COUNTRIES = (('todos','Todos'), ('ao', 'Angola'), ('mz', 'Mocambique'))
PROFILES = (('geral', 'Geral'),)

class Banner(models.Model):
    STATUS = (('ACTIVO', 'Activo'), ('INACTIVO', 'desactivo'))
    image = models.ImageField(db_column='imagem', upload_to='includes/imagens')
    image_path = models.ImageField(db_column='imagem_path')
    status = models.CharField(db_column='estado', max_length=8, blank=True, null=True, choices=STATUS, default='ACTIVO')
    created_at = models.DateTimeField(db_column='datacriacao')
    country = models.CharField(db_column='pais', max_length=5, choices=COUNTRIES)

    class Meta:
        managed = False
        db_table = 'banner'
    
    def image_tag(self):
        return mark_safe('<img src="%s" />' % (self.image_path.url))


    def __unicode__(self):
        return str(self.pk)



class News(models.Model):

    STATUS = (('activo', 'Activo'), 
             ('desactivo','Inactivo')
    )

    title = models.CharField(db_column='titulo', max_length=255)
    description = RichTextUploadingField(db_column='descricao')
    profile = models.CharField(db_column='perfil', max_length=8, blank=True, null=True, choices=PROFILES, default='geral')
    country = models.CharField(db_column='pais', max_length=5, choices=COUNTRIES, blank=False)
    category = models.CharField(db_column='tipo', max_length=11)
    image = models.TextField(db_column='imagem', blank=True, null=True)
    status = models.CharField(db_column='estado', max_length=9, blank=True, choices=STATUS, default='activo')
    created_at = models.DateTimeField(db_column='data_criacao', default=datetime.datetime.now)

    class Meta:
        managed = False
        db_table = 'noticia'
        verbose_name_plural = "news"

    def __unicode__(self):
    	return self.title

