from django.contrib import admin

from agentes_content.models import Banner, News
# Register your models here.



class BannerAdmin(admin.ModelAdmin):
    """
    
    """
    readonly_fields = ('created_at', 'image_tag')
    exclude = ('image', )
    list_display = ('__str__', 'created_at', 'status')
    list_filter = ('status', 'country')


class NewsAdmin(admin.ModelAdmin):
    """
    
    """
    exclude = ('image', 'category')
    list_display = ('__str__', 'created_at', 'status', 'country')
    search_fields = ['title']
    list_filter = ('status', 'country')



admin.site.register(Banner, BannerAdmin)
admin.site.register(News, NewsAdmin)
